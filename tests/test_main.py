"""Test file for main.py - test pipelines"""
from ..app.main import do_something, do_something_else, do_something_again, do_it, do_nothing


def test_do_something():
    """test"""
    expected = do_something(1, 2)
    actual = 3
    assert actual == expected, 'Test succeeded!'


def test_do_something_else():
    """test"""
    expected = do_something_else(1, 2)
    actual = 2
    assert actual == expected, 'Test succeeded!'


def test_do_something_again():
    """test"""
    expected = do_something_again(6, 2)
    actual = 4
    assert actual == expected, 'Test succeeded!'


def test_do_it():
    """test"""
    expected = do_it(6, 2)
    actual = 3.0
    assert actual == expected, 'Test succeeded!'


def test_do_nothing():
    """test"""
    expected = do_nothing(3, 4)
    actual = '3, 4'
    assert actual == expected, 'Test succeeded!'
