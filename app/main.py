"""file to test ci/cd pipelines with"""


def do_something(first_int: int, second_int: int) -> int:
    """add"""
    return first_int + second_int


def do_something_else(first_int: int, second_int: int) -> int:
    """multiply"""
    return first_int * second_int


def do_something_again(first_int: int, second_int: int) -> int:
    """subtract """
    return first_int - second_int


def do_it(first_int: int, second_int: int) -> float:
    """divide"""
    return float(first_int) / float(second_int)


def do_nothing(first_int: int, second_int: int) -> str:
    """do nothing"""
    return f'{first_int}, {second_int}'
